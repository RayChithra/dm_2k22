import pygame
import sys 
from PIL import Image
import random

screen_width = 800
screen_height = 600

game_start = False
level_ctr = 0
g_o = False

screen = pygame.display.set_mode((screen_width, screen_height))

class Player():

    def __init__(self):
        self.size = 50
        self.pos = [680, 450]
        self.speed = 0.2
        self.Image = pygame.image.load(r'./resources/images/character.png')
        self.Image = pygame.transform.scale(self.Image, (120, 100))
        self.rect = self.Image.get_rect()

class Background(pygame.sprite.Sprite):
    def __init__(self, image_file, location):
        pygame.sprite.Sprite.__init__(self)  #call Sprite initializer
        self.image = pygame.image.load(image_file)
        self.image = pygame.transform.scale(self.image, (screen_width, screen_height))
        self.rect = self.image.get_rect()
        self.rect.left, self.rect.top = location

class Non_playable_Character():
    def __init__(self, image_file, location, size1, size2):
        pygame.sprite.Sprite.__init__(self)  #call Sprite initializer
        self.image = pygame.image.load(image_file)
        self.image = pygame.transform.scale(self.image, (size1, size2))
        self.rect = self.image.get_rect()
        self.rect.left, self.rect.top = location
        self.speaking = False

    def draw_speech_bubble(self, text, text_color, bg_color, pos1, pos2):

        font = pygame.font.SysFont(None, 20)

        text_surface = font.render(text, True, text_color)
        text_rect = text_surface.get_rect(topleft = (pos1, pos2))

        bg_rect = text_rect.copy()
        bg_rect.inflate_ip(10,10)

        frame_rect = bg_rect.copy()
        frame_rect.inflate_ip(4,4)

        pygame.draw.rect(screen, text_color, frame_rect)
        pygame.draw.rect(screen, bg_color, bg_rect)

        screen.blit(text_surface, text_rect)

class obstacle_moving():
    def __init__(self, posx, posy, speed):
        self.image = pygame.image.load(r'./resources/images/thundercloud.png')
        self.image = pygame.transform.scale(self.image, (120, 100))
        self.speed = speed
        self.x = posx
        self.y = posy
        self.rect = self.image.get_rect()
        self.rect.left = posx
        self.rect.top = posy

    def move_1(self):
        if self.x < screen_width - self.image.get_width():
            self.x += self.speed
            self.rect.left = self.x
        else:
            self.x = 0
            self.rect.left = 0

    def move_2(self):
        if self.x > 0:
            self.x -= self.speed
            self.rect.left = self.x
        else:
            self.x = screen_width - self.image.get_width()
            self.rect.left = self.x

class obstacle_stationary():
    def __init__(self, posx, posy):
        self.image = pygame.image.load(r'./resources/images/danger.png')
        self.image = pygame.transform.scale(self.image, (120, 100))
        self.x = posx
        self.y = posy
        self.rect = self.image.get_rect()
        self.rect.left = posx
        self.rect.top = posy

pressed_right = False
pressed_left = False
pressed_up = False
pressed_down = False

cat = Player()
bg_scene_1 = Background('./resources/images/inside_house.jpeg', [0,0])
bg_scene_2 = Background('./resources/images/cloudy_bg.jpeg',[0,0])
radio = Non_playable_Character('./resources/images/radio.png',[500,450],90,60)
door = Non_playable_Character('./resources/images/door.png',[0,200], 200, 250)
obstacle_1 = obstacle_moving(0, 120, 0.2)
obstacle_2 = obstacle_moving(0, 360, 0.2)
obstacle_3 = obstacle_stationary(0, 250)
obstacle_4 = obstacle_stationary(680, 500)
dog = Non_playable_Character('./resources/images/police_dog.png',[150,250],120,100)
next = Non_playable_Character('./resources/images/next.png',[750,0],50,50)
bus = Non_playable_Character('./resources/images/bus.png',[750,0],50,50)


scene_1 = True
scene_2 = False

def set_background():
    if scene_1:
        screen.fill([255, 255, 255])
        screen.blit(bg_scene_1.image, bg_scene_1.rect)
    if scene_2:
        screen.fill([255, 255, 255])
        screen.blit(bg_scene_2.image, bg_scene_2.rect)

def check_collision(obj_1, obj_2):
    pass

def display_msg(text):
    font2 = pygame.font.Font(None, 20)
    scoretext = font2.render(text,True,(0,0,0))
    textRect = scoretext.get_rect()
    textRect.center = [400, 300]
    screen.blit(scoretext, textRect)