import pygame
import config
import sys 
from PIL import Image
import time

pygame.init()
pygame.font.init()

running = True
while running:

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running  = False

    config.screen.fill((255,255,255))
    
    # detect input for player movement
    if event.type == pygame.KEYDOWN:
        if event.key == pygame.K_LEFT:
            config.pressed_left = True
        if event.key == pygame.K_RIGHT:
            config.pressed_right = True
    if event.type == pygame.KEYUP:
        if event.key == pygame.K_LEFT:
            config.pressed_left = False
        if event.key == pygame.K_RIGHT:
            config.pressed_right = False

    if config.pressed_left and config.cat.pos[0] > 0:
        config.cat.pos[0] = config.cat.pos[0] - config.cat.speed
        config.cat.rect.left -= config.cat.speed
    if config.pressed_right and config.cat.pos[0] < config.screen_width - config.cat.Image.get_width():
        config.cat.pos[0] = config.cat.pos[0] + config.cat.speed
        config.cat.rect.left += config.cat.speed
    
    config.set_background()
    if config.scene_1:
        if abs(config.cat.pos[0] - 500) <50:
            config.radio.speaking = True
        else:
            config.radio.speaking = False
        
        if config.radio.speaking:
            text = "CYCLONE WARNING, EVACUATION IN PROGRESS"
            config.radio.draw_speech_bubble(text, (255, 255, 0), (175, 175, 0), 375, 400)

        if abs(config.cat.pos[0] - 100) < 50:
            config.door.speaking = True
        else:
            config.door.speaking = False

        if config.door.speaking:
            text = "Press up arrow to begin game"
            config.door.draw_speech_bubble(text, (255, 255, 0), (175, 175, 0), 25, 150)
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    config.game_start = True
                    config.scene_1 = False
                    config.scene_2 = True
                    config.cat.pos[0] = 400
                    config.cat.pos[1] = 500

        config.screen.blit(config.radio.image, config.radio.rect)
        config.screen.blit(config.door.image, config.door.rect)
    
    config.screen.blit(config.cat.Image, (config.cat.pos[0], config.cat.pos[1]))

    if config.game_start:

        collided = [False, False, False, False, False]

        config.set_background()

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                config.pressed_left = True
            if event.key == pygame.K_RIGHT:
                config.pressed_right = True
            if event.key == pygame.K_UP:
                config.pressed_up = True
            if event.key == pygame.K_DOWN:
                config.pressed_down = True
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT:
                config.pressed_left = False
            if event.key == pygame.K_RIGHT:
                config.pressed_right = False
            if event.key == pygame.K_UP:
                config.pressed_up = False
            if event.key == pygame.K_DOWN:
                config.pressed_down = False

        if config.pressed_left and config.cat.pos[0] > 0:
            config.cat.pos[0] = config.cat.pos[0] - config.cat.speed
            config.cat.rect.left -= config.cat.speed
        if config.pressed_right and config.cat.pos[0] < config.screen_width - config.cat.Image.get_width():
            config.cat.pos[0] = config.cat.pos[0] + config.cat.speed
            config.cat.rect.left += config.cat.speed
        if config.pressed_down and config.cat.pos[1] < config.screen_height - config.cat.Image.get_height():
            config.cat.pos[1] = config.cat.pos[1] + config.cat.speed
            config.cat.rect.top += config.cat.speed
        if config.pressed_up and config.cat.pos[1] > 0:
            config.cat.pos[1] = config.cat.pos[1] - config.cat.speed
            config.cat.rect.top -= config.cat.speed

        config.obstacle_1.move_1()
        config.obstacle_2.move_2()

        config.screen.blit(config.obstacle_1.image, config.obstacle_1.rect)
        config.screen.blit(config.obstacle_2.image, config.obstacle_2.rect)
        config.screen.blit(config.obstacle_3.image, config.obstacle_3.rect)
        config.screen.blit(config.obstacle_4.image, config.obstacle_4.rect)
        if config.level_ctr != 4:
            config.screen.blit(config.next.image, config.next.rect)
        else:
            config.screen.blit(config.bus.image, config.bus.rect)
        config.screen.blit(config.dog.image, config.dog.rect)
        config.screen.blit(config.cat.Image, (config.cat.pos[0], config.cat.pos[1]))

        if abs(config.cat.pos[0] - 150) <100 and abs(config.cat.pos[1] - 250) < 100:
            config.dog.speaking = True
        else:
            config.dog.speaking = False
        
        if config.dog.speaking:
            text = "Don't go oustide during a cyclone"
            config.dog.draw_speech_bubble(text, (255, 255, 0), (175, 175, 0), 150, 350)

        if config.level_ctr < 4:
            config.screen.blit(config.next.image, config.next.rect)
            collided[4] = config.check_collision(config.cat, config.next)
        else:
            config.screen.blit(config.bus.image, config.bus.rect)
            collided[4] = config.check_collision(config.cat, config.bus)


        # collided[0] = config.check_collision(config.cat, config.obstacle_1)
        # collided[1] = config.check_collision(config.cat, config.obstacle_2)
        # collided[2] = config.check_collision(config.cat, config.obstacle_3)
        # collided[3] = config.check_collision(config.cat, config.obstacle_4)
            
        # dist_x_cat_1 = config.cat.pos[0] - (config.cat.Image.get_width()/2)
        # dist_x_cat_2 = config.cat.pos[0] + (config.cat.Image.get_width()/2)

        # dist_x_obstacle_1_1 = config.obstacle_1.x
        # dist_x_obstacle_1_2 = config.obstacle_1.x + config.obstacle_1.image.get_width()

        # dist_x_obstacle_2_1 = config.obstacle_2.x
        # dist_x_obstacle_2_2 = config.obstacle_2.x + config.obstacle_2.image.get_width()

        # dist_x_obstacle_3_1 = config.obstacle_3.x
        # dist_x_obstacle_3_2 = config.obstacle_3.x + config.obstacle_3.image.get_width()

        # dist_x_obstacle_4_1 = config.obstacle_4.x
        # dist_x_obstacle_4_2 = config.obstacle_4.x + config.obstacle_4.image.get_width()

        # dist_y_cat_1 = config.cat.pos[1] - (config.cat.Image.get_height()/2)
        # dist_y_cat_2 = config.cat.pos[1] + (config.cat.Image.get_height()/2)

        # dist_y_obstacle_1_1 = config.obstacle_1.y
        # dist_y_obstacle_1_2 = config.obstacle_1.y + config.obstacle_1.image.get_height()

        # dist_y_obstacle_2_1 = config.obstacle_2.y
        # dist_y_obstacle_2_2 = config.obstacle_2.y + config.obstacle_2.image.get_height()

        # dist_y_obstacle_3_1 = config.obstacle_3.y
        # dist_y_obstacle_3_2 = config.obstacle_3.y + config.obstacle_3.image.get_height()

        # dist_y_obstacle_4_1 = config.obstacle_4.y
        # dist_y_obstacle_4_2 = config.obstacle_4.y + config.obstacle_4.image.get_height()

        # if dist_x_cat_1 > dist_x_obstacle_1_1 and dist_x_cat_1 < dist_x_obstacle_1_2:
        #     if dist_y_cat_1 > dist_y_obstacle_1_1 and dist_y_cat_1 < dist_y_obstacle_1_2:
        #         collided[0] = True


        for i in range(4):
            if collided[i] == True:
                config.g_o = True
        if collided[4] == True:
            config.level_ctr += 1
            config.obstacle_1.speed += 0.1
            config.obstacle_2.speed += 0.1
            

    if config.g_o:
        config.screen.fill([255,255,255])
        config.display_msg("GAME OVER")

    if not config.g_o and config.game_start and config.level_ctr == 4:
        config.screen.fill([255,255,255])
        config.display_msg("SUCCESFULLY EVACUATED")

    pygame.display.update()

pygame.quit()